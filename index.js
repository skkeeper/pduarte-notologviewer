#! /usr/bin/env node
/* jshint asi: true */

'use strict'

var readLine = require('readline')
var path = require('path')

var request = require('request')
var chalk = require('chalk')
var Enumerable = require('linq')
var ansi = require('ansi')
var moment = require('moment')

var cursor = ansi(process.stdout)
cursor.hide()

var home = (process.env.HOME || process.env.USERPROFILE) + '/.notologviewer'
var config = require(path.join(home, 'config.json'))

if (process.platform === 'win32') {
  var rl = readLine.createInterface({
    input: process.stdin,
    output: process.stdout
  })

  rl.on('SIGINT', function () {
    process.emit('SIGINT')
  })
}

process.on('SIGINT', function () {
  // graceful shutdown
  cursor.show()
  process.exit()
})

var lastRefresh = new Date()

function requestLogs () {
  request.get(config.viewUrl, {
    'auth': {
      'user': config.couchDbUsername,
      'pass': config.couchDbPassword,
      'sendImmediately': false
    }
  },
  function (error, response, body) {
    if (!error && response.statusCode === 200) {
      var jsonBody = JSON.parse(body)

      if (jsonBody.rows.length === 0) {
        return
      }

      jsonBody.rows = Enumerable.from(jsonBody.rows).where(function (t) {
        var timestamp = new Date(t.value.Timestamp)
        if (timestamp > lastRefresh) {
          return true
        }

        return false
      }).toArray()

      if (jsonBody.rows.length === 0) {
        return
      }

      lastRefresh = new Date(jsonBody.rows[0].value.Timestamp)

      for (var i = jsonBody.rows.length - 1; i >= 0; i--) {
        var rowValue = jsonBody.rows[i].value
        var time = new Date(rowValue.Timestamp)
        var timestamp = moment(time).format('YYYY-MM-DD HH:mm:ss')
        var renderedTimestamp = chalk.green(timestamp + ': ')

        if (rowValue.Properties.Database === 'totoEntitiesDevelopment') {
          renderedTimestamp = chalk.grey(timestamp + ': ')
        }

        var message = rowValue.RenderedMessage
        var renderedMessage = message

        if (message.toLowerCase().indexOf('bug') > -1 ||
          rowValue.Level === 'Fatal') {
          renderedMessage = chalk.black.bgRed.bold(message)
        } else if (rowValue.Level === 'Error') {
          renderedMessage = chalk.yellow.bold(message)
        } else if (rowValue.Level === 'Verbose') {
          renderedMessage = chalk.cyan(message)
        }

        console.log(renderedTimestamp + renderedMessage)
      }
    }
  })
}

requestLogs()
setInterval(requestLogs, 5000)
